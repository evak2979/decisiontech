# Basket/Discount editor #

A quick introduction to our custom basket/discount program

### How does it work ###

Each new product (butter, bread, milk, etc) inherits from a base product abstract class. Due to the lack of a repository for now, product prices are hard coded. In the future, we should consider retrieving such information from a data store. Once all our products are in a basket, 
we can ask for the final price by calling the
    
	CalculateFinalPrice()
	
method on the basket item. Internall, the basket item uses an IDiscount factory which returns a pipeline (similar to how middleware works) of IDiscount implementations. Each IDiscount implementation receives the basket's products, calculates the total discount and adds it to the discount 
of the next IDiscount in line. Once there are no more IDiscount implementations, the total discount is returned to the basket instance.

The basket then returns the final price, by calculating the total value of every item in the basket and subtracting the total value of all the discounts.

### How do I get set up? ###

The project includes unit/integration tests. The DecisionTech.Domain includes all the necessary files and interfaces to get started. (Suggested that Dependency Injection is used). 
Given how the requirements have not specificed the need for a front end, none has been included for the sake of returning the actual basket implementation back to you ASAP. If, however, one is required kindly let me know and I can provide a RestFUL API on top of the solution.

### Testing ###
* Unit tests included for each business class (Basket/Discount calculators)
* Integration tests included without stubs/mocks, so as to properly test the flow from start to end. All test cases provided with the requirements have been tested and pass successfully.

### TODOs ###

* Logging
* Performance Analytics
* Moving pricing information to datastore
* Deciding on what is the right way to provide the information (RESTFul web API suggested)

### Anything more required ###

Please reach out to me if you wish me to add to the readme file, or discuss anything more prior to your review. Many thanks.