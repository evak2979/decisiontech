using DecisionTech.Domain;
using DecisionTech.Domain.Interfaces.Implementations;
using DecisionTech.Domain.Models;
using DecisionTech.TestHelpers;
using Shouldly;
using Xunit;

namespace DecisionTech.Tests.Integration
{
    public class BasketIntegrationTests
    {
        private Basket _sut;

        public BasketIntegrationTests()
        {
            _sut = new Basket(new DiscountFactory());
        }

        [Fact]
        public void GivenOneBreadOneButterAndOneMilk_WhenCalculatingPrice_ShouldReturnCorrectDiscountedPrice()
        {
            // given
            _sut.AddProductsToBasket(Helpers.CreateListOfSpecificProduct<Bread>(1));
            _sut.AddProductsToBasket(Helpers.CreateListOfSpecificProduct<Butter>(1));
            _sut.AddProductsToBasket(Helpers.CreateListOfSpecificProduct<Milk>(1));
            decimal correctDiscountedPrice = 2.95m;

            // when
            var result = _sut.CalculateFinalPrice();

            // then
            result.ShouldBe(correctDiscountedPrice);
        }

        [Fact]
        public void GivenTwoBreadsAndTwoButters_WhenCalculatingPrice_ShouldReturnCorrectDiscountedPrice()
        {
            // given
            _sut.AddProductsToBasket(Helpers.CreateListOfSpecificProduct<Bread>(2));
            _sut.AddProductsToBasket(Helpers.CreateListOfSpecificProduct<Butter>(2));
            var correctDiscountedPrice = 3.10m;

            // when
            var result = _sut.CalculateFinalPrice();

            // then
            result.ShouldBe(correctDiscountedPrice);
        }

        [Fact]
        public void GivenFourMilk_WhenCalculatingPrice_ShouldReturnCorrectDiscountedPrice()
        {
            {
                // given
                _sut.AddProductsToBasket(Helpers.CreateListOfSpecificProduct<Milk>(4));
                var correctDiscountedPrice = 3.45m;

                // when
                var result = _sut.CalculateFinalPrice();

                // then
                result.ShouldBe(correctDiscountedPrice);
            }
        }

        [Fact]
        public void GivenTwoButterOneBreadAndEightilk_WhenCalculatingPrice_ShouldReturnCorrectDiscountedPrice()
        {
            {
                // given
                _sut.AddProductsToBasket(Helpers.CreateListOfSpecificProduct<Butter>(2));
                _sut.AddProductsToBasket(Helpers.CreateListOfSpecificProduct<Bread>(1));
                _sut.AddProductsToBasket(Helpers.CreateListOfSpecificProduct<Milk>(8));
                var correctDiscountedPrice = 9.00m;

                // when
                var result = _sut.CalculateFinalPrice();

                // then
                result.ShouldBe(correctDiscountedPrice);
            }
        }
    }
}
