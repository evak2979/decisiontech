﻿using System;
using System.Collections.Generic;

namespace DecisionTech.TestHelpers
{
    public static class Helpers
    {
        public static List<T> CreateListOfSpecificProduct<T>(int listSize)
        {
            var listOfProducts = new List<T>();

            for (int i = 0; i < listSize; i++)
            {
                listOfProducts.Add(Activator.CreateInstance<T>());
            }

            return listOfProducts;
        }
    }
}
