﻿using System.Collections.Generic;
using System.Linq;
using DecisionTech.Domain.Interfaces;
using DecisionTech.Domain.Models;

namespace DecisionTech.Domain
{
    public class Basket
    {
        private readonly IDiscount _discount;

        public Basket(IDiscountFactory discountFactory)
        {
            _discount = discountFactory.Create();
            Products = new List<Product>();
        }

        private List<Product> Products { get; }

        public void AddProductsToBasket<T>(List<T> products) where T : Product
        {
            if (products != null)
                Products.AddRange(products);
        }

        public decimal CalculateFinalPrice()
        {
            var basketPrice = Products.Sum(x => x.Price);

            if (_discount == null)
                return basketPrice;

            return basketPrice - _discount.Calculate(Products);
        }
    }
}