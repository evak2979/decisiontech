﻿namespace DecisionTech.Domain.Models
{
    public class Butter : Product
    {
        public override decimal Price => 0.80m;
    }
}