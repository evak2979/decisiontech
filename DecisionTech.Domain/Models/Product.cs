﻿namespace DecisionTech.Domain.Models
{
    public abstract class Product
    {
        public abstract decimal Price { get;  }
    }
}