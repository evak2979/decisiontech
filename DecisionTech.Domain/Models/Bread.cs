﻿namespace DecisionTech.Domain.Models
{
    public class Bread : Product
    {
        public override decimal Price => 1.00m;
    }
}