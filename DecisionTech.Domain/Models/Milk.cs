﻿namespace DecisionTech.Domain.Models
{
    public class Milk : Product
    {
        public override decimal Price => 1.15m;
    }
}