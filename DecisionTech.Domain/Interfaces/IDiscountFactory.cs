﻿namespace DecisionTech.Domain.Interfaces
{
    public interface IDiscountFactory
    {
        IDiscount Create();
    }
}