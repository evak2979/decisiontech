﻿using System.Collections.Generic;
using DecisionTech.Domain.Models;

namespace DecisionTech.Domain.Interfaces
{
    public interface IDiscount
    {
        decimal Calculate(List<Product> products);
    }
}