﻿using System.Collections.Generic;
using System.Linq;
using DecisionTech.Domain.Models;

namespace DecisionTech.Domain.Interfaces.Implementations
{
    public class BreadDiscount : IDiscount
    {
        private readonly IDiscount _nextDiscount;
        private const int NumberOfButterItemsRequiredForDiscount = 2;
        private decimal discountOnBreadPrice = 0.5m;

        public BreadDiscount(IDiscount nextDiscount = null)
        {
            _nextDiscount = nextDiscount;
        }

        public decimal Calculate(List<Product> products)
        {
            var discount = 0.00m;

            if (_nextDiscount != null)
            {
                discount += _nextDiscount.Calculate(products);
            }

            if (products == null || products.Count < NumberOfButterItemsRequiredForDiscount)
            {
                return discount;
            }

            if (!products.Any(x => x is Bread))
            {
                return discount;
            }

            var numberOfAvailableBreadDiscounts = products.Count(x => x is Butter) / NumberOfButterItemsRequiredForDiscount;
            var numberOfBreadItems = products.Count(x => x is Bread);

            var numberOfDiscountedBreads = 0;

            for (int i = 0; i < numberOfAvailableBreadDiscounts && i < numberOfBreadItems; i++)
            {
                numberOfDiscountedBreads++;
            }

            // I am not particularly fond of having to new a Bread item to get the price here... But in production ready code
            // I am assuming the price would be coming from a repository of sorts, and we would use it as such. I've newed/used the .Price here
            // for convenience purposes.
            discount += numberOfDiscountedBreads * new Bread().Price * discountOnBreadPrice;

            return discount;
        }
    }
}
