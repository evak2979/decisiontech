﻿using System.Collections.Generic;
using System.Linq;
using DecisionTech.Domain.Models;

namespace DecisionTech.Domain.Interfaces.Implementations
{
    public class MilkDiscount : IDiscount
    {
        private readonly IDiscount _nextDiscount;
        private const int NumberOfMilkItemsRequiredForDiscount = 4;

        public MilkDiscount(IDiscount nextDiscount = null)
        {
            _nextDiscount = nextDiscount;
        }

        public decimal Calculate(List<Product> products)
        {
            var discount = 0.00m;

            if (_nextDiscount != null)
            {
                discount += _nextDiscount.Calculate(products);
            }

            if (products == null || products.Count < NumberOfMilkItemsRequiredForDiscount)
            {
                return discount;
            }

            var totalFreeBottlesOFMilk = products.Where(x => x is Milk).Count() / NumberOfMilkItemsRequiredForDiscount;

            // I am not particularly fond of having to new a Milk item to get the price here... But in production ready code
            // I am assuming the price would be coming from a repository of sorts, and we would use it as such. I've newed/used the .Price here
            // for convenience purposes.
            return discount + totalFreeBottlesOFMilk * new Milk().Price;
        }
    }
}