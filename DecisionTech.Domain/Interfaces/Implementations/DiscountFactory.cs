﻿namespace DecisionTech.Domain.Interfaces.Implementations
{
    public class DiscountFactory : IDiscountFactory
    {
        public IDiscount Create()
        {
            return new BreadDiscount(new MilkDiscount());
        }
    }
}