using System.Collections.Generic;
using System.Linq;
using DecisionTech.Domain;
using DecisionTech.Domain.Interfaces;
using DecisionTech.Domain.Models;
using Xunit;
using DecisionTech.TestHelpers;
using Moq;
using Shouldly;

namespace DecisionTech.Tests.Unit
{
    public class BasketUnitTests
    {
        private Basket _sut;
        private Mock<IDiscountFactory> _mockDiscountFactory;
        private List<Milk> milkItems = Helpers.CreateListOfSpecificProduct<Milk>(1);
        private List<Bread> breadItems = Helpers.CreateListOfSpecificProduct<Bread>(1);
        private List<Butter> butterItems = Helpers.CreateListOfSpecificProduct<Butter>(1);

        public BasketUnitTests()
        {
            _mockDiscountFactory = new Mock<IDiscountFactory>();
        }

        [Fact]
        public void GivenANumberOfProducts_WhenCallingGetFinalPriceAndThereAreNoDiscounts_ShouldReturnFinalPrice()
        {
            // given
            _mockDiscountFactory.Setup(x => x.Create()).
                Returns<IDiscount>(null);

            _sut = new Basket(_mockDiscountFactory.Object);
            _sut.AddProductsToBasket( CreateProductsForBasket());

            // when
            var price = _sut.CalculateFinalPrice();

            // then
            price.ShouldBe(milkItems.First().Price + breadItems.First().Price + butterItems.First().Price);
        }

        [Fact]
        public void GivenANumberOfProducts_WhenCallingGetFinalPriceAndThereAreDiscounts_ShouldReturnDiscountedPrice()
        {
            // given
            var totalDiscount = 1;
            var mockDiscount = new Mock<IDiscount>();
            mockDiscount.Setup(x => x.Calculate(It.IsAny<List<Product>>()))
                .Returns(totalDiscount);

            _mockDiscountFactory.Setup(x => x.Create()).
                Returns(mockDiscount.Object);

            _sut = new Basket(_mockDiscountFactory.Object);
            _sut.AddProductsToBasket(CreateProductsForBasket());

            // when
            var price = _sut.CalculateFinalPrice();

            // then
            price.ShouldBe(milkItems.First().Price + breadItems.First().Price + butterItems.First().Price - totalDiscount);
        }

        private List<Product> CreateProductsForBasket()
        {
            var products = new List<Product>();

            milkItems = Helpers.CreateListOfSpecificProduct<Milk>(1);
            breadItems = Helpers.CreateListOfSpecificProduct<Bread>(1);
            butterItems = Helpers.CreateListOfSpecificProduct<Butter>(1);

            products.AddRange(milkItems);
            products.AddRange(breadItems);
            products.AddRange(butterItems);

            return products;
        }
    }
}
