﻿using System.Collections.Generic;
using DecisionTech.Domain.Interfaces;
using DecisionTech.Domain.Interfaces.Implementations;
using DecisionTech.Domain.Models;
using DecisionTech.TestHelpers;
using Moq;
using Shouldly;
using Xunit;

namespace DecisionTech.Tests.Unit
{
    public class BreadDiscountTests
    {
        private BreadDiscount _sut;

        public BreadDiscountTests()
        {
            _sut = new BreadDiscount();
        }

        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        public void GivenLessThanTwoButters_WhenCalculatingDiscount_ShouldReturnZero(int numberOfButterItems)
        {
            // given
            var butterItems = Helpers.CreateListOfSpecificProduct<Butter>(numberOfButterItems);

            // when
            var discount = _sut.Calculate(new List<Product>(butterItems));

            // then
            discount.ShouldBe(0);
        }

        [Fact]
        public void GivenTwoOrMoreButterItemsAndNoBreadItems_WhenCalculatingDiscount_ShouldReturnZero()
        {
            // given
            var butterItems = Helpers.CreateListOfSpecificProduct<Butter>(3);

            // when
            var discount = _sut.Calculate(new List<Product>(butterItems));

            // then
            discount.ShouldBe(0);
        }

        [Theory]
        [InlineData(2, 1, 1)]
        [InlineData(2, 2, 1)]
        [InlineData(3, 1, 1)]
        [InlineData(4, 1, 1)]
        [InlineData(4, 2, 2)]
        public void GivenTwoOrMoreButterItemsAndOneOrMoreBreadItems_WhenCalculatingDiscount_ShouldReducePriceOfBreadItemToHalfForEachTwoButterITems(int numberOfButterItems, int numberOfBreadItems, int expectedDiscountedItems)
        {
            // given
            var butterItems = Helpers.CreateListOfSpecificProduct<Butter>(numberOfButterItems);
            var breadItems = Helpers.CreateListOfSpecificProduct<Bread>(numberOfBreadItems);
            var products = new List<Product>(butterItems);
            products.AddRange(breadItems);

            // when
            var discount = _sut.Calculate(products);

            // then
            discount.ShouldBe(expectedDiscountedItems * new Bread().Price * 0.5m);
        }

        [Fact]
        public void GivenBreadAndButterItems_IfNextDiscountIsNotNull_ShouldAddNextDiscountToCurrentOneBeforeReturning()
        {
            // given
            var butterItems = Helpers.CreateListOfSpecificProduct<Butter>(1);
            var breadItems = Helpers.CreateListOfSpecificProduct<Bread>(1);
            var products = new List<Product>(butterItems);
            products.AddRange(breadItems);
            var nextDiscountAmount = 1;
            var iDiscount = new Mock<IDiscount>();
            iDiscount.Setup(x => x.Calculate(products)).Returns(nextDiscountAmount);

            _sut = new BreadDiscount(iDiscount.Object);

            // when
            var discount = _sut.Calculate(products);

            // then
            discount.ShouldBe(nextDiscountAmount);
        }
    }
}
