﻿using System.Collections.Generic;
using DecisionTech.Domain.Interfaces;
using DecisionTech.Domain.Interfaces.Implementations;
using DecisionTech.Domain.Models;
using DecisionTech.TestHelpers;
using Moq;
using Shouldly;
using Xunit;

namespace DecisionTech.Tests.Unit
{
    public class MilkDiscountTests
    {
        private MilkDiscount _sut;

        public MilkDiscountTests()
        {
            _sut = new MilkDiscount();
        }

        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        public void GivenLessThanFourMilks_WhenCalculatingDiscount_ShouldReturnZero(int numberOfMilkItems)
        {
            // given
            var butterItems = Helpers.CreateListOfSpecificProduct<Butter>(numberOfMilkItems);

            // when
            var discount = _sut.Calculate(new List<Product>(butterItems));

            // then
            discount.ShouldBe(0);
        }

        [Theory]
        [InlineData(4)]
        [InlineData(7)]
        [InlineData(8)]
        public void GivenFourOrMoreMilks_WhenCalculatingDiscount_ShouldReturnTheSumOfPricesForEveryForthMilk(int numberOfMilkItems)
        {
            // given
            var milkItems = Helpers.CreateListOfSpecificProduct<Milk>(numberOfMilkItems);

            // when
            var discount = _sut.Calculate(new List<Product>(milkItems));

            // then
            discount.ShouldBe(numberOfMilkItems/4 * new Milk().Price);
        }

        [Fact]
        public void GivenMilkItems_IfNextDiscountIsNotNull_ShouldAddNextDiscountToCurrentOneBeforeReturning()
        {
            // given
            var milkItems = Helpers.CreateListOfSpecificProduct<Milk>(1);
            var products = new List<Product>(milkItems);
            
            var nextDiscountAmount = 1;
            var iDiscount = new Mock<IDiscount>();
            iDiscount.Setup(x => x.Calculate(products)).Returns(nextDiscountAmount);

            _sut = new MilkDiscount(iDiscount.Object);

            // when
            var discount = _sut.Calculate(products);

            // then
            discount.ShouldBe(nextDiscountAmount);
        }
    }
}
